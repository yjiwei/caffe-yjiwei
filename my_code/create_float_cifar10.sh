#!/usr/bin/env sh
# This script converts the cifar data into leveldb format.

EXAMPLE=data/cifar100_preprocessing
DATA=data/cifar100_preprocessing
DBTYPE=lmdb

echo "Creating $DBTYPE..."

# rm -rf $EXAMPLE/preprocessed_cifar10_train_$DBTYPE $EXAMPLE/preprocessed_cifar10_test_$DBTYPE

./build/examples/cifar10/convert_cifar_float_data.bin $DATA $EXAMPLE $DBTYPE

# echo "Computing image mean..."

# ./build/tools/compute_image_mean -backend=$DBTYPE \
#   $EXAMPLE/cifar10_train_$DBTYPE $EXAMPLE/mean.binaryproto

echo "Done."
