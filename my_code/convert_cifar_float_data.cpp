//
// This script converts the CIFAR dataset to the leveldb format used
// by caffe to perform classification.
// Usage:
//    convert_cifar_data input_folder output_db_file
// The CIFAR dataset could be downloaded at
//    http://www.cs.toronto.edu/~kriz/cifar.html

#include <fstream>  // NOLINT(readability/streams)
#include <string>

#include "boost/scoped_ptr.hpp"
#include "glog/logging.h"
#include "google/protobuf/text_format.h"
#include "stdint.h"

#include "caffe/proto/caffe.pb.h"
#include "caffe/util/db.hpp"
#include "caffe/util/format.hpp"

using caffe::Datum;
using boost::scoped_ptr;
using std::string;
namespace db = caffe::db;

const int kCIFARSize = 32;
const int kCIFARImageNBytes = 3072;
const int kCIFARBatchSize = 10000;
const int kCIFARTrainBatches = 5;
const int KCIFARImageNFloatBytes = 3072 * 4;
const int KMaxKeyLength = 256;

void read_image(std::ifstream* file, int* label, char* buffer) {
	char label_char;
	file->read(&label_char, 1);
	*label = label_char;
	file->read(buffer, kCIFARImageNBytes);
	return;
}

//read label and float data into datum
void read_image_float(std::ifstream* file, int* label, Datum* datum){
	datum->clear_data();
	datum->clear_float_data();
    float label_char;
    file->read((char*)&label_char, sizeof(label_char));
    *label = static_cast<int>(label_char);
	datum->set_label(*label);
	const int size_float = sizeof(float);
	//float32 is 4 bytes per data
	for (int i = 0; i < kCIFARImageNBytes; i++){
		float value;
		file->read((char*)&value, size_float);
		datum->add_float_data(value);
	}
}


void convert_dataset_float (const string& input_folder, const string& output_folder,
	const string& db_type) {
	scoped_ptr<db::DB> train_db(db::GetDB(db_type));
	train_db->Open(output_folder + "/pre_cifar100_train_" + db_type, db::NEW);
	scoped_ptr<db::Transaction> txn(train_db->NewTransaction());
	// Data buffer
	int label;
	Datum datum;
	datum.set_channels(3);
	datum.set_height(kCIFARSize);
	datum.set_width(kCIFARSize);

	LOG(INFO) << "Writing Training data";
	for (int fileid = 0; fileid < kCIFARTrainBatches; ++fileid) {
		// Open files
		LOG(INFO) << "Training Batch " << fileid + 1;
        string batchFileName = input_folder + "/pre_data_fine_lable_batch_" + caffe::format_int(fileid+1) + ".bin";
		std::ifstream data_file(batchFileName.c_str(),
			std::ios::in | std::ios::binary);
		CHECK(data_file) << "Unable to open train file #" << fileid + 1;
		for (int itemid = 0; itemid < kCIFARBatchSize; ++itemid) {
			read_image_float(&data_file, &label, &datum);
			string out;
			CHECK_EQ(datum.float_data_size(), datum.channels() * datum.height() * datum.width()) <<"Incorrect data size";
			CHECK(datum.SerializeToString(&out));
            txn->Put(caffe::format_int(fileid * kCIFARBatchSize + itemid, 5), out);
		}
	}
	//check data and label
//	std::cout<<"Datum label: "<<datum.label() <<", size: "<<sizeof(datum.label())<<std::endl;
//	std::cout<<"Datum size: "<<datum.float_data_size()<<std::endl;	
	txn->Commit();
	train_db->Close();

	LOG(INFO) << "Writing Testing data";
	scoped_ptr<db::DB> test_db(db::GetDB(db_type));
	test_db->Open(output_folder + "/pre_cifar100_test_" + db_type, db::NEW);
	txn.reset(test_db->NewTransaction());
	// Open files
	std::ifstream data_file((input_folder + "/pre_test_fine_lable_batch.bin").c_str(),
		std::ios::in | std::ios::binary);
	CHECK(data_file) << "Unable to open test file.";
	for (int itemid = 0; itemid < kCIFARBatchSize; ++itemid) {
		read_image_float(&data_file, &label, &datum);
		string out;
		CHECK(datum.SerializeToString(&out));
        txn->Put(caffe::format_int(itemid, 5), out);
	}
    std::cout<<"label: "<<label<<", size: "<<sizeof(label)<<std::endl;    //
    std::cout<<"Datum data: "<<datum.float_data(0) <<", size: "<<datum.float_data_size()<<std::endl;	
	txn->Commit();
	test_db->Close();
}

int main(int argc, char** argv) {
	if (argc != 4) {
		printf("This script converts the CIFAR dataset to the leveldb format used\n"
			"by caffe to perform classification.\n"
			"Usage:\n"
			"    convert_cifar_data input_folder output_folder db_type\n"
			"Where the input folder should contain the binary batch files.\n"
			"The CIFAR dataset could be downloaded at\n"
			"    http://www.cs.toronto.edu/~kriz/cifar.html\n"
			"You should gunzip them after downloading.\n");
	}
	else {
		google::InitGoogleLogging(argv[0]);
		FLAGS_logtostderr = 1;
		convert_dataset_float(string(argv[1]), string(argv[2]), string(argv[3]));
	}
	return 0;
}
