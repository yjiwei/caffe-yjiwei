﻿http://caffe.berkeleyvision.org/install_apt.html

先安装NVIDIA显卡驱动（375.66有问题，最好用357.39）
Ubuntu系统集成的显卡驱动程序是nouveau，
我们需要先将nouveau从linux内核卸载掉才能安装NVIDIA官方驱动。 
将nouveau添加到黑名单blacklist.conf中
# 按ctrl+alt+F1进入tty文本模式进入下面的操作

sudo vim /etc/modprobe.d/blacklist.conf

# 在文件末尾添加如下几行：
blacklist vga16fb 
blacklist nouveau 
blacklist rivafb 
blacklist rivatv 
blacklist nvidiafb

#保存，退出，然后再更新一下内核
sudo update-initramfs -u

# 重启系统 按ctrl+alt+F1进入tty文本模式 
重启系统确认nouveau是已经被屏蔽掉，使用lsmod命令查看：
lsmod | grep nouveau

使用如下命令添加Graphic Drivers PPA

sudo add-apt-repository ppa:graphics-drivers/ppa
sudo apt-get update

寻找合适的驱动版本

ubuntu-drivers devices

关闭(图形)桌面显示管理器LightDM
sudo service lightdm stop

安装nvidia driver,(378,384)代表驱动版本号，选一个，最好选推荐版本

sudo apt-get install nvidia-(378,384)
sudo reboot

重启系统后，执行下面的命令查看驱动的安装状态显示安装成功

sudo nvidia-smi
sudo nvidia-settings


##############################################################################
然后安装CUDA8.0和cudnn5.1

sudo chmod u+x cuda_8.0.61_375.26_linux.run 
sudo ./cuda_8.0.61_375.26_linux.run
在此过程中第一个选择是是否安装驱动，一定选择否，因为已经装过驱动

安装完毕后，再声明一下环境变量，并将其写入到 ~/.bashrc 的尾部:
sudo gedit ~/.bashrc

export PATH=/usr/local/cuda-8.0/bin${PATH:+:${PATH}}
export LD_LIBRARY_PATH=/usr/local/cuda-8.0/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}

source  ~/.bashrc

5.测试cuda的samples

cd ~/NVIDIA_CUDA-8.0_Samples/1_Utilities/deviceQuery
make
./deviceQuery 
# 如果报错，尝试用root权限运行 sudo ./deviceQuery
最终显示pass  证明CUDA安装成功

将cudnn5.1 解压到cuda，然后安装：
cd Downloads/cuda/
sudo cp cuda/include/cudnn.h /usr/local/cuda/include/
sudo cp cuda/lib64/libcudnn* /usr/local/cuda/lib64/
sudo chmod a+r /usr/local/cuda/include/cudnn.h
sudo chmod a+r /usr/local/cuda/lib64/libcudnn*

#安装375驱动后会有部分问题，
#解决办法  
sudo mv /usr/lib/nvidia-375/libEGL.so.1 /usr/lib/nvidia-375/libEGL.so.1.org
sudo mv /usr/lib32/nvidia-375/libEGL.so.1 /usr/lib32/nvidia-375/libEGL.so.1.org
sudo ln -s /usr/lib/nvidia-375/libEGL.so.375.39 /usr/lib/nvidia-375/libEGL.so.1
sudo ln -s /usr/lib32/nvidia-375/libEGL.so.375.39 /usr/lib32/nvidia-375/libEGL.so.1

Rebooted afterwards, then the problem was solved an Steam started again.


###############################################################################
安装caffe所需要的依赖项：

sudo apt-get install -y build-essential cmake git pkg-config

sudo apt-get install libprotobuf-dev libleveldb-dev libsnappy-dev libopencv-dev libhdf5-serial-dev protobuf-compiler

sudo apt-get install --no-install-recommends libboost-all-dev

sudo apt-get install libatlas-base-dev 

sudo apt-get install libgflags-dev libgoogle-glog-dev liblmdb-dev

sudo apt-get install -y python-pip

sudo apt-get install python-dev 

sudo apt-get install -y python-numpy python-scipy

安装opencv

安装依赖项：

sudo apt-get install build-essential

sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev

sudo apt-get install python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev
sudo apt-get install libgtk-3-dev libqt4-dev ffmpeg
sudo apt-get install x264 v4l-utils unzip

cd ~/opencv-3.1.0/
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_TBB=ON -D WITH_V4L=ON -D WITH_QT=ON -D WITH_OPENGL=ON -D WITH_GTK=ON -D WITH_GTK_2_X=ON -D CUDA_NVCC_FLAGS="-D_FORCE_INLINES" ..
解决方案：需要修改一处源码：opencv/modules/cudalegacy/src/
在graphcuts.cpp中将

#if !defined (HAVE_CUDA) || defined (CUDA_DISABLER) 

改为

#if !defined (HAVE_CUDA) || defined (CUDA_DISABLER) || (CUDART_VERSION >= 8000)
sudo make -j8

#出错：No rule to make target '/usr/lib/x86_64-linux-gnu/libGL.so', needed by 'lib/libopencv_cudev.so.3.1.0'. Stop.
#解决办法：
Problem with libGL.so on 64-bit Ubuntu

I have an Nvidia card and as expected I installed Nvidia drivers for my ubuntu. But, whenever I compiled programs that used OpenGL ( libGL.so ) I ran into compile errors similar to:

No rule to make target `/usr/lib/x86_64-linux-gnu/libGL.so'

I was sure there was some problem with my libGL.so file so I ended up looking at it properties and found out that it was a broken link pointing to 'mesa/libGL.so' which in turn was pointing to 'libGL.so.1' which never existed.

#******bpaluri3@bpaluri3:~/libfreenect/build$ ls -l /usr/lib/x86_64-linux-gnu/libGL.so 
#lrwxrwxrwx 1 root root 13 2011-08-10 04:20 /usr/lib/x86_64-linux-gnu/libGL.so -> mesa/libGL.so

#******bpaluri3@bpaluri3:~/libfreenect/build$ ls -l /usr/lib/x86_64-linux-gnu/mesa/libGL.so 
#lrwxrwxrwx 1 root root 10 2011-08-10 04:20 /usr/lib/x86_64-linux-gnu/mesa/libGL.so -> libGL.so.1


Then I verified the libGL.so.1 in my /usr/lib directory and found out that it pointed to libGL.so.290.10 which is the file provided by the nvidia driver.

#******bpaluri3@bpaluri3:~/libfreenect/build$ ls -l /usr/lib/libGL.so.1 
#lrwxrwxrwx 1 root root 15 2012-01-09 13:53 /usr/lib/libGL.so.1 -> libGL.so.290.10

I just overwrote the symbolic link in `/usr/lib/x86_64-linux-gnu/libGL.so' with the nvidia openGL library i.e. `/usr/lib/libGL.so' ( I had to delete the old symbolic link before I do this ).

#******bpaluri3@bpaluri3:~/libfreenect/build$ sudo rm /usr/lib/x86_64-linux-gnu/libGL.so 
#******bpaluri3@bpaluri3:~/libfreenect/build$ sudo ln -s /usr/lib/libGL.so.1 /usr/lib/x86_64-linux-gnu/libGL.so 

The final two statements are the fixes for the libGL error and I hope anyone who runs into a similar issue will find this information useful.

#错误解决 ************************************************************************

sudo make install -j8

#安装caffe***********************************************

git clone https://github.com/BVLC/caffe.git

cd caffe/
cp Makefile.config.example Makefile.config
sudo gedit Makefile.config

重要 :
将# Whatever else you find you need goes here.下面的
INCLUDE_DIRS := $(PYTHON_INCLUDE) /usr/local/include
LIBRARY_DIRS := $(PYTHON_LIB) /usr/local/lib /usr/lib
修改为：
INCLUDE_DIRS := $(PYTHON_INCLUDE) /usr/local/include /usr/include/hdf5/serial
LIBRARY_DIRS := $(PYTHON_LIB) /usr/local/lib /usr/lib /usr/lib/x86_64-linux-gnu /usr/lib/x86_64-linux-gnu/hdf5/serial
(因为ubuntu16.04的文件包含位置发生了变化)

(6)修改makefile文件
打开makefile文件，做如下修改：
将(大概415行的样子)：
NVCCFLAGS +=-ccbin=$(CXX) -Xcompiler-fPIC $(COMMON_FLAGS)
替换为：
NVCCFLAGS += -D_FORCE_INLINES -ccbin=$(CXX) -Xcompiler -fPIC $(COMMON_FLAGS)
这里写图片描述

在Makefile文件的第大概181行，把 hdf5_hl和hdf5修改为hdf5_serial_hl 和 hdf5_serial，即
将：
LIBRARIES += glog gflags protobuf boost_system boost_filesystem m hdf5_hl hdf5
改为：
LIBRARIES += glog gflags protobuf boost_system boost_filesystem m hdf5_serial_hl hdf5_serial   暂时没用到

make all -j8
make test -j8
make runtest -j8 (可能出错)   原因是安装opencv后没有重启 重启电脑  再运行这句  就行了
#如果重启不行 .build_release/tools/caffe: error while loading shared libraries: libopencv_core.so.3.1: cannot open shared object file: No such file or directory

make clean
sudo ldconfig
继续重新make 就行了

编译pycaffe

sudo apt-get install python-numpy python-scipy python-matplotlib python-sklearn python-skimage python-h5py python-protobuf python-leveldb python-networkx python-nose python-pandas python-gflags cython ipython

sudo apt-get install protobuf-c-compiler protobuf-compiler -y

cd ~/caffe
make pycaffe
sudo gedit ~/.bashrc
添加  export PYTHONPATH=/home/yjiwei/caffe/python:$PYTHONPATH

source ~/.bashrc

 
# 电脑卡死现象 
尝试去掉显示器VGA接口，改用DVI接口，(可能为主要原因，好像HDMI也不行)
并升级华硕主板BOIS





